const http = require('http');
const fs = require('fs');
const path = require('path')
const TWO_HUNDRED = 200;
const FOUR_HUNDRED = 400;
const PORT = 8080;
const READ_ERR_CODE = 'ENOENT';

function checkFileFolder(){
    return new Promise((resolve, reject) => {
        fs.access(path.join(__dirname, 'files'), (err) => {
            if(err){
                reject(err)
            } else {
                resolve()
            }
        })
    })
}

function getLogs(){
    return new Promise((resolve, reject) => {
        fs.readFile('./logs.json', 'utf8', (err, content) => {
            if(err){
                reject(err)
            } else {
                resolve(JSON.parse(content))
            }
        })
    })
}

function logInfo(type, name){
    const currentDate = Date.now();
    const templates = {
        post: `File with name '${name}' saved, time: ${currentDate}`,
        get: `File '${name}' was accessed, time: ${currentDate}`,
        fail: `There was an attempt to access file that does not exist, time: ${currentDate}`
    }
    
    getLogs().then(data => {
        if (name !== undefined){
        const message = templates[type]
        data.logs.push({message});
        fs.writeFile('./logs.json', JSON.stringify(data), (err) => {
            if (err) {
                throw err;
            }
        })
        }
    }).catch(err => console.log(err))   
}

checkFileFolder().catch(err => {
    if(err.code === READ_ERR_CODE){
        fs.mkdir(path.join(__dirname, 'files'), err => {
            if(err){
                console.log(err)
            }
        })
    } else {
        console.log(err)
    }
})

getLogs().catch((err) => {
    if(err.code === READ_ERR_CODE){
        const boilerPlate = {
            logs: []
        }
        fs.writeFile('./logs.json', JSON.stringify(boilerPlate), (err) => {
            if(err){
                throw err
            }
        })
    }
})

module.exports = () => {
    http.createServer((req, res) => {
        const { pathname, searchParams: queries }= new URL(req.url, `http://${req.host}`)
        if (req.method === 'POST'){
            if(!queries.get('filename') || !queries.get('content')){
                res.writeHead(FOUR_HUNDRED, {'Content-type':'text-html'});
                res.end('Invalid query data');
            } else if(!queries.get('filename').split('.')[1]){
                res.writeHead(FOUR_HUNDRED, {'Content-type':'text-html'});
                res.end('Invalid file format');
            } else {
                fs.writeFile(`./files/${queries.get('filename')}`,queries.get('content'), (err) => {
                    if(err){
                        throw err;
                    }
                    res.writeHead(TWO_HUNDRED, {'Content-type':'text-html'});
                    res.end('Success');
                    logInfo('post', queries.get('filename'))
            })
            }
        } else if(req.method === 'GET'){
            const [path1, path2] = pathname.split('/').slice(1)
            if(path1 === 'logs'){
                getLogs().then( data => {
                        if(queries.get('from') && queries.get('to')){
                            const logs = data.logs.filter(val => {
                                const timeStamp = parseInt(val.message.split(':')[1]);
                                return timeStamp >=queries.get('from') && timeStamp <= queries.get('to');
                            })
                            res.writeHead(TWO_HUNDRED, {'Content-type':'text/plain'})
                            res.end(JSON.stringify({logs}))
                        } else {
                            res.writeHead(TWO_HUNDRED, {'Content-type':'text/plain'})
                            res.end(JSON.stringify(data))
                        }
                    }).then(() => logInfo('get', 'logs.json')).catch(err => console.log(err))
            } else if(path1 === 'file'){
                fs.readFile(`files/${path2}`, (err, content) => {
                    if(err){
                        res.writeHead(FOUR_HUNDRED, {'Content-type':'text-html'});
                        res.end('No such file in the directory');
                        logInfo('fail', '') ;
                    } else {
                        res.writeHead(TWO_HUNDRED, {'Content-type':'text/json'})
                        res.end(content);
                        logInfo('get', path2) ;
                    }
                })
            }
        }
    }).listen(PORT, () => console.log(`Server is live on port ${PORT}`))
}
